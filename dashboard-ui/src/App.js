import React from 'react';
import Workspace from './workspace/Workspace';
import { Provider } from "react-redux";
import {applyMiddleware, combineReducers, createStore} from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import activitySagas from "./activities/activitySagas";
import createSagaMiddleware from "redux-saga";
import { reducer as recentReducer } from "./activities/recentActivitiesReducer";
import { reducer as activityReducer } from "./activities/activityReducer";
import { reducer as profileReducer } from "./accounts/accountReducer";
import { reducer as requestStatusReducer } from "./workspace/requestStatusReducer";

import { BrowserRouter as Router } from "react-router-dom";
import accountSagas from "./accounts/accountSagas";

export const AppConfig = React.createContext({});

function configureStore() {
  const sagaMiddleware = createSagaMiddleware();
  const composeEnhancers = composeWithDevTools({});
  const store = createStore(combineReducers({
    "recent" : recentReducer,
    "activity" : activityReducer,
    "profile" : profileReducer,
    "status" : requestStatusReducer,
  }), composeEnhancers(applyMiddleware(sagaMiddleware)));
  sagaMiddleware.run(activitySagas);
  sagaMiddleware.run(accountSagas);
  return store;
}

function App(props) {
  return (
    <AppConfig.Provider value={props} >
      <Provider store={configureStore()}>
        <Router>
          <Workspace/>
        </Router>
      </Provider>
    </AppConfig.Provider>
  );
}

export default App;
