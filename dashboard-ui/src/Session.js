import { call} from "@redux-saga/core/effects";
import { callEndpoint} from "./APIUtils";
import { login } from "./accounts/accountSagas";

const mergeOptions = function(defaultOptions, options) {
  if(options.headers) {
    const headers = {...defaultOptions.headers, ...options.headers};
    return {...defaultOptions, ...options, headers}
  } else {
    return {...defaultOptions, ...options}
  }
}

class Session {

  static instance = undefined;

  constructor(token) {
    this.options = {
      headers: {
        "Authorization" : `bearer ${token}`
      }
    };
    Session.instance = this;
  }

  doGet(endpoint, options={}) {
    return callEndpoint(endpoint, mergeOptions(Session.instance.options, options));
  }

  doPost(endpoint, options={}) {
    return callEndpoint(endpoint, mergeOptions(Session.instance.options, options), "POST");
  }

  doPut(endpoint, options={}) {
    return callEndpoint(endpoint, mergeOptions(Session.instance.options, options), "PUT");
  }

  doDelete(endpoint, options={}) {
    return callEndpoint(endpoint, mergeOptions(Session.instance.options, options), "DELETE");
  }
}

export function getSession() {
  if(Session.instance) {
    return Promise.resolve(Session.instance);
  }
  return call(login, { payload: { "authProvider": "refresh" }});
}

export function refreshSession(token) {
  console.log("refresh session");
  return new Session(token);
}
