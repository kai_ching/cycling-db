export function formatDate(dateString) {
  const date = new Date(dateString);
  const dateTimeFormat = new Intl.DateTimeFormat('en', {
    year: 'numeric', month: 'short', day: 'numeric',
    hour: '2-digit', minute: '2-digit', hour12: false
  });
  const [{ value: month },,{ value: day },,{ value: year },,{value: hour},,{value:minute}] = dateTimeFormat.formatToParts(date);
  return `${day} ${month} ${year} at ${hour}:${minute}`;
}

export function formatDuration(seconds, maxUnit) {
  if(maxUnit === undefined) {
    if(seconds > 86400) {
      maxUnit = "d";
    } else if(seconds > 3600) {
      maxUnit = "h";
    } else if(seconds > 60) {
      maxUnit = "m";
    }
  }

  const duration = {
    "days" : 0,
    "hours" : 0,
    "minutes" : 0,
    "seconds" : seconds
  }

  switch(maxUnit) {
    case "d" : {
      duration.days = Math.floor(duration.seconds/ 86400);
      duration.seconds -= duration.days * 86400;
    }
    case "h" : {
      duration.hours = Math.floor(duration.seconds / 3600);
      duration.seconds -= duration.hours * 3600;
    }
    case "m" : {
      duration.minutes = Math.floor(duration.seconds/ 60);
      duration.seconds -= duration.minutes * 60;
    }
  }
  switch(maxUnit) {
    case "d": return `${duration.days}d ${duration.hours}h ${duration.minutes}m`;
    case "h": return `${duration.hours}h ${duration.minutes}m`;
    case "m" : return `${duration.minutes}m ${duration.seconds}s`;
    default: return `${seconds}`
  }
}

export function formatDistance(metres, targetUnit) {
  if(!targetUnit) {
    targetUnit = "m";
  }
  switch(targetUnit) {
    case "m" : return `${metres}m`;
    case "km" : return `${(metres/1000).toFixed(2)}km`;
    case "ft" : return `${(metres * 3.28084).toFixed(1)}ft`;
    case "mi" : return `${(metres/ 1609.34).toFixed(2)}mi`;
  }
}

export function formatSpeed(metresPerSec, targetUnit) {
  switch(targetUnit) {
    case "km/h" : return `${(metresPerSec * 3.6).toFixed(1)}km/h`;
    case "mi/h" : return `${(metresPerSec/ 1609.34 * 3600).toFixed(1)}mi/h`
    case "m/s" : return `${metresPerSec.toFixed(0)}m/s`;
  }
}

export function formatCadence(cadence) {
  return `${cadence} rpm`;
}

export function formatEnergy(kcal, targetUnit) {
  switch (targetUnit) {
    case "kJ" : return `${(kcal * 4.184).toFixed(0)}kJ`;
    case "kcal" : return `${kcal}kcal`;
  }
}

export function formatPercentage(number) {
  return `${(number * 100).toFixed(0)}%`;
}

export function formatPower(watt) {
  return `${watt.toFixed(0)}W`;
}

export function formatHeartRate(beatsPerMinute) {
  return `${beatsPerMinute.toFixed(0)}bpm`;
}
