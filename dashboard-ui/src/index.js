import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';

const root = document.getElementById('root');
const configNode = root.querySelector('[rel="configuration"]');
const config = configNode ? JSON.parse(configNode.innerHTML) : {}

ReactDOM.render(
  <React.StrictMode>
    <App {...config} />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
