const initialState = {
  page: 0,
  list: []
}

export const ActionTypes = {
  FETCH_RECENT : 'recentRequest',
  FETCH_RECENT_SUCCESS : 'recentSuccess',
  FETCH_RECENT_FAIL : 'recentFailure'
}

export const reducer = (state = initialState, action) => {
  const {type, payload} = action;
  switch(type) {
    case ActionTypes.FETCH_RECENT_SUCCESS : {
      state = { ...state,
        page : payload.length > 0 ? state.page + 1 : state.page,
        list : state.list.length > 0 ? [...state.list, ...payload] : payload,
      };
      break;
    }

    default: {}
  }
  return state;
}


