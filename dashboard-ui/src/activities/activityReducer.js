const initialState = {
  selected: null
}

export const ActionTypes = {
  FETCH_ACTIVITY: "activityRequest",
  FETCH_SUCCESS: "activitySuccess",
  FETCH_FAIL: "activityFailure"
}

export const reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch(type) {
    case ActionTypes.FETCH_SUCCESS : {
      state = { ...state,
        selected: payload
      };
      break;
    }
    default: { return state; }
  }
}
