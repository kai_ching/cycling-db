import {call, put, takeLatest, all} from "redux-saga/effects";
import { ActionTypes as RecentActionTypes} from './recentActivitiesReducer';
import { ActionTypes as ActivityActionTypes} from "./activityReducer";
import {getSession} from "../Session";

function* fetchActivity(activityEvent) {
  try {
    const session = yield getSession();
    console.log("session", session);
    const { activityId} = activityEvent.payload;
    const activity = yield call(session.doGet, `/api/activities/${activityId}`);

    yield put( {
      type: ActivityActionTypes.FETCH_SUCCESS,
      payload: activity
    });
  } catch(error) {
    console.error(error);
    yield put({
      type: ActivityActionTypes.FETCH_FAIL,
      payload: error
    });
  }
}

function* fetchRecent(pageEvent) {
  try {
    const session = yield getSession();
    const {page} = pageEvent.payload;
    const activities = yield call(session.doGet, `/api/activities?page=${page}&pageSize=1`);
    yield put({
      type: RecentActionTypes.FETCH_RECENT_SUCCESS,
      payload: activities
    });
  } catch(error) {
    console.error(error);
    yield put({
      type: RecentActionTypes.FETCH_RECENT_FAIL,
      payload: error
    });
  }
}

function* watchRecentActivities() {
  yield takeLatest(RecentActionTypes.FETCH_RECENT, fetchRecent);
}
function* watchFetchActivity() {
  yield takeLatest(ActivityActionTypes.FETCH_ACTIVITY, fetchActivity);
}

export default function* rootSaga() {
  yield all([
    watchRecentActivities(),
    watchFetchActivity()
  ]);
}
