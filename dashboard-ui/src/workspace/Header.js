import React from "react";
import NavBar from "../components/Nav/Nav";
import NavButton from "../components/Nav/NavButton";


function Header(props) {
  const { toggleNavMenu, ...rest} = props;
  return (
    <header>
      <div className="header-content">
        <div className="header-section"/>
        <NavBar {...rest}/>
        <div className="header-section">
          <NavButton onClick={toggleNavMenu}/>
        </div>
      </div>
    </header>
  );
}

export default Header;
