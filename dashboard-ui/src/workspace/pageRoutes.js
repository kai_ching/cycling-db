import RecentPage from "./pages/activity/RecentPage";
import UploadPage from "./pages/activity/upload/UploadPage";
import ActivityPage from "./pages/activity/ActivityPage";
import LoginPage from "./pages/account/LoginPage";
import MultiPageContent from "./MultiPageContent";

import RouteDetail from "./pages/planning/RouteDetailPage";
import RoutesSumamry from "./pages/planning/RoutesSummaryPage";
import RegisterPage from "./pages/account/RegisterPage";

export const activityRoutes = [
  {
    path: "/activities/recent",
    label: "Recent",
    pageContent: RecentPage
  },
  {
    path: "/activities/upload",
    label: "Upload",
    pageContent: UploadPage
  },
  {
    path: "/activities/:activityId",
    props: {
      navGroup: "activities"
    },
    pageContent: ActivityPage
  }
];

export const planningRoutes = [
  {
    path: "/planning/routes",
    pageContent: RouteDetail
  }
]

export const pageRoutes = [
  {
    path: "/login",
    props: {
      registerUrl: "/register",
      loginSuccessUrl: "/activities/recent"
    },
    pageContent: LoginPage
  },
  {
    path: "/register",
    props: {
      successUrl: "/activities/recent",
      submitUrl: "/api/register"
    },
    pageContent: RegisterPage
  },
  {
    path: "/activities",
    label: "Activities",
    props: {
      navGroup: "activities",
      routes: activityRoutes,
      defaultRoutePath: "/activities"
    },
    pageContent: MultiPageContent
  },
  {
    path: "/planning",
    label: "Planning",
    props: {
      routes: planningRoutes,
      defaultRoutePath: "/planning"
    },
    pageContent: RouteDetail
  }
]

export const defaultRoute = "/login";
