import React, { useState } from "react";
import {CircleMarker, Map, Polyline, TileLayer} from "react-leaflet";
import { TextField } from "@material-ui/core";
import { isPointOnLine } from "../../../MathUtils";

const RouteDetail = ({center =[0, 0], zoom = 4 }) => {

  const [waypoints, setWaypoints] = useState([]);
  const [selectedWaypoint, setSelectedWaypoint] = useState(undefined);

  const handleAddWaypoint = event => {
    if(!selectedWaypoint) {
      console.log("handle add waypoint");
      const waypoint = {
        latlng: event.latlng
      }
      if(waypoints.length > 0) {
        setWaypoints([...waypoints, waypoint]);
      } else {
        setWaypoints([waypoint]);
      }
    } else {
      setSelectedWaypoint(undefined);
    }
  }

  const handleSelectWaypoint = event => {
    console.log("handle select waypoint", event);
    if(!selectedWaypoint) {
      const [selectedWaypoint] = waypoints.filter((waypoint,index) =>
        (waypoint.latlng.lat === event.latlng.lat &&
        waypoint.latlng.lng === event.latlng.lng));
      setSelectedWaypoint(selectedWaypoint);
    }
    return false;
  }

  const handleMoveWaypoint = event => {
    if(selectedWaypoint) {
      selectedWaypoint.latlng = event.latlng;
      setWaypoints([...waypoints]);
    }
  }

  const handleInsertWaypoint = event => {
    console.log("handle insert waypoint", event);
    const point = {
      latlng : event.latlng
    }
    const map = event.target._map;
    waypoints.forEach((waypoint, index) => {
      if(index > 0 && selectedWaypoint == null) {
        const p1 = map.latLngToContainerPoint(waypoints[index - 1].latlng);
        const p2 = map.latLngToContainerPoint(waypoints[index].latlng);
        const line = {
          x1: p1.x,
          y1: p1.y,
          x2: p2.x,
          y2: p2.y,
        }
        if(isPointOnLine(event.containerPoint, line)) {
          const newWayPoints = [...waypoints];
          newWayPoints.splice(index, 0, point);
          setSelectedWaypoint(point);
          setWaypoints(newWayPoints);
        }
      }
    })
  }

  const polylinePoints = waypoints.map(waypoint => waypoint.latlng);

  return (
    <div>
      <div>
        <form>
          <div className="form-field">
            <TextField label="Title" name="title" variant="outlined"/>

          </div>
        </form>
      </div>
      <Map dragging={selectedWaypoint == null} center={center} zoom={zoom} style={{width: "100%", height: "600px"}}
           onClick={handleAddWaypoint}
           onMouseMove={handleMoveWaypoint}>
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        { polylinePoints.length > 1 ? (
          <Polyline positions={polylinePoints} color="red"
                    onMouseDown={handleInsertWaypoint}
          />
        ) : null }

        { polylinePoints.length > 0 ? (
          polylinePoints.map((latlng, index) =>
            <CircleMarker key={index} center={latlng} radius={5} color="red" fillOpacity={1}
                          onMouseDown={handleSelectWaypoint}
            />)) : null }
      </Map>

    </div>);
}

export default RouteDetail;
