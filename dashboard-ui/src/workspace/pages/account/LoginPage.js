import React, {useLayoutEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "@material-ui/core";
import { useHistory } from "react-router-dom";

import MuiAlert from '@material-ui/lab/Alert';

import Login from "../../../components/Login/Login";
import {ActionTypes as AccountActionTypes} from "../../../accounts/accountReducer";

import "./LoginPage.scss";

function Alert(props) {
  return <MuiAlert elevation={2} variant="filled" square {...props} />;
}

const LoginPage = ({registerUrl, forgotPasswordUrl, loginSuccessUrl}) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const submitting = useSelector(store => store.status.loading.indexOf("login") > -1);
  const signedIn = useSelector(store => store.profile.signedIn);
  const error = useSelector(store => store.status.errors["login"]);

  const loginAction = (username, password) => {
    dispatch({
      type: AccountActionTypes.LOGIN,
      payload: {
        authProvider: "local",
        username, password
      }
    });
  }

  useLayoutEffect(() => {
    if(signedIn) {
      history.push(loginSuccessUrl);
    }
  }, [signedIn, history]);

  if(signedIn) {
    return null;
  } else {
    return (
      <div className="login-content">
        { error ? (
          <Alert severity="error">{error.message}</Alert>
        ) : null}

        <Login loginAction={loginAction} forgotPasswordUrl={forgotPasswordUrl} submitting={submitting}/>
        <div className="login-footer">
          { registerUrl ? (
            <Link href={registerUrl}>Not a member?</Link>
          ) : null}
        </div>
      </div>
    );
  }
}

export default LoginPage;
