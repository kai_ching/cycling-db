import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import {callEndpoint} from "../../../APIUtils";
import Alert from "@material-ui/lab/Alert";
import RegisterForm from "../../../components/RegisterForm/RegisterForm";

const RegisterPage = ({successUrl, submitUrl}) => {

  const history = useHistory();
  const [error, setError] = useState(undefined);

  const cancelAction = () => {
    history.back();
  }

  const submitAction = (username, password, profile) => {
    const payload = {
      username, password, profile
    }
    return callEndpoint(submitUrl, {
      body: payload
    }, "POST")
      .then(() => {
        history.push(successUrl)
      })
      .catch(error => {
        setError(error);
      });
  }

  return (
    <div className="register-content">
      { error ? (
        <Alert severity="error">{error.message}</Alert>
      ): null}

      <RegisterForm submitAction={submitAction} cancelAction={cancelAction}/>
    </div>
  )
}

export default RegisterPage;
