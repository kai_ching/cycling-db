import React, {useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import CircularProgress from "@material-ui/core/CircularProgress";

import { ActionTypes } from "../../../activities/activityReducer";
import ActivityDetail from "../../../components/ActivityDetail/ActivityDetail";

import "./ActivityPage.scss";

function ActivityPage() {
  const { activityId } = useParams();
  const dispatch = useDispatch();
  const activity = useSelector(store => store.activity.selected);

  useEffect(() => {
    if(activityId) {
      dispatch({
        type: ActionTypes.FETCH_ACTIVITY,
        payload: {activityId }
      });
    }
  }, [ dispatch, activityId ]);

  if(activity) {
    return (
      <>
        <ActivityDetail activity={activity}/>
      </>
    );
  } else {
    return (
      <div className="load-indicator" >
        <CircularProgress />
      </div>
    )
  }
}

export default ActivityPage;
