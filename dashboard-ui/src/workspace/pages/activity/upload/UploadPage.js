import React, {Fragment, useState} from "react";
import FileUpload from "../../../../components/fileupload/FileUpload";
import {Form, Formik} from 'formik';
import UploadDetails from "./UploadDetails";
import FileParser from "./FileParser";

import "./UploadPage.scss";

function UploadPage() {
  const [file, setFile] = useState(null);
  const [activity, setActivity] = useState(null);

  async function onSubmit (details) {
    const formData = new FormData();
    formData.append('detail',
      new Blob([JSON.stringify(details)], {
      type : 'application/json'
    }));
    formData.append('file', new Blob([file]), file.name);
    return fetch('/api/activities', {
      method: 'post',
      body: formData
    })
      .then(response => response.json())
      .then(response => {
        window.location.href = '/activities/' + response.id;
      })
  }

  const onFileUpload = file => {
    if(file && file.name.endsWith('.fit')) {
      setFile(file);
      FileParser.parse(file)
        .then(activity => {
          setActivity(activity);
        });
    }
  }

  const validate = (values) => {
    const errors = {};
    if(!values.title) {
      errors.title = 'Please enter a title for the activity'
    }
    return errors
  };

  const formContent = (activity == null) ?
    (<Fragment>
      <h4>Please upload your activity in a .fit file format</h4>
      <FileUpload onFileChanged={file => onFileUpload(file)}/>
    </Fragment>)
     : <UploadDetails activity={activity}/>;

  return (
    <Fragment>
      <h2>Add new activity</h2>
      <Formik validate={ validate }
              onSubmit={ onSubmit }
              initialValues={{title : '', description : ''}}>
        <Form className="activity-create-form">
          {formContent}
        </Form>
      </Formik>
    </Fragment>
  );
}

export default UploadPage;
