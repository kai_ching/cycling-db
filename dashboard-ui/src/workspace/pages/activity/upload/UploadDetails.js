import React from 'react';
import {Field} from "formik";
import {Map, TileLayer, Polyline} from "react-leaflet";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {FormControl} from "@material-ui/core";


const UploadDetails = (props) => {
  const { activity } = props;
  const mapCentre = [
    (activity.minLat + activity.maxLat) / 2,
    (activity.minLng + activity.maxLng) / 2
  ];

  const mapStyles = {
    width: '100%',
    height: '100%'
  }

  const latLngList =
    activity.streams.get('Latitude').map((lat, index) => ([
      lat, activity.streams.get('Longitude')[index]
    ]));

  return (
    <div className="activity-detail">
      <div className="thumbnail map">
        <Map center={mapCentre} zoom={13} zoomControl={false}
             boxZoom={false} doubleClickZoom={false} dragging={false} keyboard={false} scrollWheelZoom={false} tap={false} touchZoom={false}
             style={mapStyles} >
          <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          />
          {/*<TileLayer*/}
          {/*  url="https://api.mapbox.com/styles/v1/kai-ching/ckdnni2ex48kq1imphmhf5dlo/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1Ijoia2FpLWNoaW5nIiwiYSI6ImNrZG5uY202OTB0bnkzMG1nNnkwbTc1ZncifQ.88mQntahokjTiP-BCCZfBg"*/}
          {/*  attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"*/}
          {/*/>*/}
          <Polyline positions={latLngList} color="red"/>
        </Map>
      </div>
      <div className="fields">
        <FormControl margin="normal" fullWidth>
          <Field type="text" name="title">
            {({ field, form: { touched, errors }}) => (
              <TextField {...field} required label="Title"
                         error={touched.title && errors.title}
                         helperText={errors.title}
              />
            )}
          </Field>
        </FormControl>
        <FormControl margin="normal" fullWidth>
          <Field type="text" name="description">
            {({ field }) => (
              <TextField {...field} multiline label="Description" variant="outlined" rows="6"/>
            )}
          </Field>
        </FormControl>
      </div>
      <div className="buttons">
        <Button variant="contained" color="primary" type="submit">
          Create
        </Button>
      </div>
    </div>
  );
};

export default UploadDetails;
