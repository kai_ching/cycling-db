import { SportsLib } from '@sports-alliance/sports-lib';

function getActivityStat(activity, statName) {
  if(activity.stats.get(statName)) {
    return activity.stats.get(statName).value;
  }
  return undefined;
}

function parseStreams(activity) {
  const allowableStreamNames = ['Latitude', 'Longitude'];
  const allowableStreams = activity.streams.filter(stream => {
    return allowableStreamNames.indexOf(stream.type) > -1
  });
  const maxStreamLength = allowableStreams.map(stream => stream.data.length).reduce((result, length) => Math.max(result, length), 0);
  const timestamps = [];
  const result =new Map();

  for(let i = 0; i < maxStreamLength; i++) {
    const shouldInclude = allowableStreams
      .map(stream => stream.data[i])
      .reduce((result, streamValue) => result || (streamValue !== undefined && streamValue !== null), false);
    if(shouldInclude) {
      timestamps.push(i);
    }
  }

  allowableStreams.forEach(stream => {
    const data = [];
    timestamps.forEach(time => { data.push(stream.data[time])});
    result.set(stream.type, data);
  });
  result.set('timestamps', timestamps);
  return result;
}

function parse(file) {
  if(file && file.name.endsWith('.fit')) {
    return file.arrayBuffer()
      .then(data => SportsLib.importFromFit(data))
      .then(event => ({
        startTime: event.activities[0].startDate,
        endTime : event.activities[0].endDate,
        movingTime: getActivityStat(event.activities[0], 'Moving time'),
        idleTime: getActivityStat(event.activities[0], 'Pause Time'),
        distance: getActivityStat(event.activities[0], 'Distance'),
        averageSpeed: getActivityStat(event.activities[0], 'Average Speed'),
        maximumSpeed: getActivityStat(event.activities[0], 'Maximum Speed'),
        averageCadence: getActivityStat(event.activities[0], 'Average Cadence'),
        maximumCadence: getActivityStat(event.activities[0], 'Maximum Cadence'),
        ascent: getActivityStat(event.activities[0], 'Ascent'),
        descent: getActivityStat(event.activities[0], 'Descent'),
        energy: getActivityStat(event.activities[0], 'Energy'),
        averageTemperature: getActivityStat(event.activities[0], 'Average Temperature'),
        streams: parseStreams(event.activities[0])
      }))
      .then(activity => {
        activity.minLat = activity.streams.get('Latitude').reduce((minLat, val) => (minLat === undefined ? val :  Math.min(val, minLat)), undefined);
        activity.maxLat = activity.streams.get('Latitude').reduce((maxLat, val) => (maxLat === undefined ? val :  Math.max(val, maxLat)), undefined);
        activity.minLng = activity.streams.get('Longitude').reduce((minLng, val) => (minLng === undefined ? val :  Math.min(val, minLng)), undefined);
        activity.maxLng = activity.streams.get('Longitude').reduce((maxLng, val) => (maxLng === undefined ? val : Math.max(val, maxLng)), undefined);
        return activity;
      });
  }
}

export default {
  parse: parse
}
