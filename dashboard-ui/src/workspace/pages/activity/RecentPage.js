import React, {useCallback} from "react";
import {useDispatch, useSelector} from "react-redux";
import {ActionTypes} from "../../../activities/recentActivitiesReducer";
import ActivitySummary from "../../../components/ActivitySummary/ActivitySummary";
import InfiniteScroll from "../../../components/InfiniteScroll/InfiniteScroll";

import "./RecentPage.scss";

function RecentPage() {
  const dispatch = useDispatch();
  const page = useSelector(store => store.recent.page);

  const activities = useSelector(store => store.recent.list);
  const isLoading = useSelector(store => store.status.loading.indexOf("recent") > -1);

  const fetchMore = useCallback(() => {
    dispatch({
      type: ActionTypes.FETCH_RECENT,
      payload: { page }
    });
  }, [page]);

  return (
    <>
      <h2>Your recent activities</h2>
      <div className="activities">
        <InfiniteScroll items={ activities } fetchMore={ fetchMore } isLoading={isLoading}>
          <ActivitySummary item="activity"/>
        </InfiniteScroll>
      </div>

    </>
  )
}

export default RecentPage;
