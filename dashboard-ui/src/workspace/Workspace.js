import React, {useCallback, useState} from "react";
import { Switch, Route, Redirect, useLocation } from "react-router-dom";
import { Drawer } from "@material-ui/core";

import {pageRoutes, defaultRoute} from "./pageRoutes";
import Header from "./Header.js";
import Footer from "./Footer.js";

import './workspace.scss';

function Workspace() {
  const location = useLocation();
  const [drawerOpen, setDrawerOpen] = useState(false);

  const pageClass= `page-${location.pathname.substring(location.pathname.lastIndexOf('/') + 1)}`;

  const toggleNavMenu = (open) => {
    console.log("toggle menu");
    if(open === undefined) {
      open = !drawerOpen;
    }
    setDrawerOpen(open);
  };

  return (
    <div className={pageClass}>
      <Switch>
        {
          pageRoutes.map((route, index) =>
            <Route key={index}
                   path={route.path}
                   exact={route.exact !== undefined ? route.exact : (!(route.props && route.props.routes))}
                   render={() =>
                     React.createElement(WorkspaceBody, {...route.props, content: route.pageContent, toggleNavMenu: toggleNavMenu})}
            />)
        }
        <Route exact path="/">
          <Redirect to={defaultRoute} />
        </Route>
      </Switch>
      <Footer/>
      <Drawer anchor="right" open={drawerOpen} onClose={() => {toggleNavMenu(false)} }>
        Hello world
      </Drawer>
    </div>
  )
}

function WorkspaceBody(props) {
  const { content, hideHeader, navGroup, toggleNavMenu, ...contentProps} = props;
  return (
    <>
      { !hideHeader ? <Header routes={pageRoutes} selected={navGroup} toggleNavMenu={toggleNavMenu}/> : null}
      <div className="workspace-content">
        { React.createElement(content, contentProps) }
      </div>
    </>
  )
}

export default Workspace;
