import React from "react";
import Nav from "../components/Nav/Nav";
import {Redirect, Route, Switch} from "react-router-dom";

import "./MultiPageContent.scss";

function MultiPageContent({routes, defaultRoutePath, beforeNav, afterNav}) {
  return (
    <div className="multi-page-content">
      <div className="side-bar">
        { beforeNav ? (
          <div class="before-nav">{beforeNav}</div>
        ) : null}
        <div className="page-nav">
          <Nav routes={routes} variant="NavMenu"/>
        </div>
        { afterNav ? (
          <div class="after-nav">{afterNav}</div>
        ) : null}
      </div>
      <div className="page-content">
        <Switch>
          {
            routes.map((route, index) =>
              <Route key={index} path={route.path} exact={true}
                     render={() => React.createElement(route.pageContent)}/>)
          }
          <Route exact path={defaultRoutePath}>
            <Redirect to={routes[0].path} />
          </Route>
        </Switch>
      </div>
    </div>
 )

}

export default MultiPageContent;
