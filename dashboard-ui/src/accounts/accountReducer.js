const initialState = {
  signedIn: false,
  permissions: 0,
  displayName: ""
}

export const ActionTypes = {
  LOGIN: "loginRequest",
  LOGIN_SUCCESS: "loginSuccess",
  LOGIN_FAIL: "loginFailure",
  PROFILE_UPDATE: "profileUpdate"
}

export const reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch(type) {
    case ActionTypes.PROFILE_UPDATE : {
      if(payload == null) {
        state = initialState;
      } else {
        const {permissions, displayName} = payload;
        state = {...state, signedIn: true, permissions, displayName};
      }
      break;
    }
    default: {}
  }
  return state;
}
