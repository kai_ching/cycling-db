import {put, call, takeLatest} from "redux-saga/effects";

import {callEndpoint} from "../APIUtils";
import { ActionTypes as AccountActionTypes } from "./accountReducer.js";
import { refreshSession } from "../Session";

export function* login(loginEvent) {
  console.log("Login");
  try {
    const loginResponse = yield call(callEndpoint, "/api/login",
      { body: loginEvent.payload }, "POST");

    if(loginResponse) {
      const {token, profile} = loginResponse;
      yield put({
        type: AccountActionTypes.LOGIN_SUCCESS
      });
      yield put({
        type: AccountActionTypes.PROFILE_UPDATE,
        payload: profile
      });
      return refreshSession(token);
    }
  } catch (error) {
    console.error(error);
    yield put({
      type: AccountActionTypes.LOGIN_FAIL,
      payload: error
    });
    yield put({
      type: AccountActionTypes.PROFILE_UPDATE,
      payload: null
    });
  }
}

export default function* watchLoginActivities() {
  yield takeLatest(AccountActionTypes.LOGIN, login);
}
