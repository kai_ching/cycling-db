import React, {useEffect, useRef, useState} from 'react';
import { CircularProgress } from "@material-ui/core";
import { debounce } from "lodash";

function InfiniteScroll({ items, fetchMore, isLoading, children }) {
  const contentRef = useRef();
  const [lastScroll, setLastScroll] = useState(0);

  useEffect(() => {
    fetchMore();
  }, []);

  useEffect(() => {
    function handleScrollEvent(wheelEvent) {
      wheelEvent.stopPropagation();

      const now = Date.now();
      if(contentRef.current && now - lastScroll > 2000) {
        const bodyRect = document.querySelector("body").getBoundingClientRect();
        const boundingRect =  contentRef.current.getBoundingClientRect();
        if(!isLoading && boundingRect.bottom < bodyRect.height) {
          fetchMore();
        }
        setLastScroll(Date.now());
      }
    }
    const listener = debounce(handleScrollEvent, 200, {leading: true, trailing: false});
    window.addEventListener("wheel", listener);
    return (() => { window.removeEventListener("wheel", listener)});
  }, [fetchMore, lastScroll]);

  const itemPropName = children.props.item;

  return (
    <>
      <div ref={contentRef}>
        {
          items.map((item, index) => (
            React.Children.map(children, child => {
              const childProps = { index };
              childProps[itemPropName] = item;
              return React.cloneElement(child, childProps);
            })
          ))
        }
      </div>
      <div className="text-center" style={{marginBottom: "4px"}}>
        <CircularProgress size={24} color="secondary" style={{visibility: isLoading ? "visible" : "hidden"}}/>
      </div>
    </>
  )
}

export default InfiniteScroll;
