import {ResponsiveContainer, LineChart, XAxis, YAxis, Line, ReferenceLine} from "recharts";
import React, {useState, useEffect, useMemo} from "react";
import { throttle } from "lodash";

function extract(streams, indexedBy) {
  const result = [];
  // const indexStream = streams[indexedBy].filter((val, index) =>  index === 0 || val !== streams[indexedBy][index -1]);

  streams[indexedBy].forEach((indexValue, index) => {
    const dataPoint = {};
    dataPoint[indexedBy] = indexValue;
    const attributes = Object.keys(streams);
    attributes.splice(attributes.indexOf(indexedBy), 1);
    attributes.forEach(attribute => {
      dataPoint[attribute] = streams[attribute][index];
    });
    result.push(dataPoint);
  });
  return result;
}

const Chart = ({activity}) => {
  const [state, setState] = useState({
    xAxis: "Distance",
    mark: undefined
  });

  const data = useMemo(() => {
    let streams = {
      "Distance" : activity.streams.Distance.map(val => val / 1000),
      "Speed" : activity.streams.Speed.map(val => val * 3.6),
      "Cadence": activity.streams.Cadence,
      "Altitude" : activity.streams.Altitude
    }
    return extract(streams, state.xAxis);
  }, [activity, state.xAxis]);

  // const throttled = throttle(event => {
  //   mouseMoveHandler(event, state, setState);
  // }, 200);

  return (
    <div>
      <LineChart data={data} height={70} width={600}
                 margin={{ top: 20, right: 0, left: 0, bottom: 0 }}>
        <YAxis />
        <Line type="monotoneX" dataKey="Speed" stroke="#8884d8" dot={false}/>
        {state.mark ? <ReferenceLine x={state.mark }/> : null}
      </LineChart>
      {/*<LineChart data={data} height={70} width={600}*/}
      {/*           margin={{ top: 0, right: 0, left: 0, bottom: 20 }}>*/}
      {/*  <YAxis />*/}
      {/*  <Line type="monotoneX" dataKey="Cadence" stroke="#8884d8" dot={false}/>*/}
      {/*  {state.mark ? <ReferenceLine x={Math.round(state.mark * data.length)}/> : null}*/}
      {/*</LineChart>*/}
    </div>
  )
}

export default Chart;
