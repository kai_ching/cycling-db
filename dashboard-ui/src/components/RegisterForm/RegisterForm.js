import React from "react";
import { Form, Formik } from "formik";
import {FormControl, Paper, TextField} from "@material-ui/core"

const RegisterForm = ({submitAction, cancelAction}) => {

  const initialFormValues = {
    username: "",
    password: "",
    confirmPassword: "",
    profile: {
      email: ""
    }
  }
  const validate = () => {
    return true;
  }

  return (
    <Paper className="register" square elevation={2}>
      <Formik validate={ validate }
              onSubmit={ data => submitAction(...data) }
              initialValues={initialFormValues}>
        <Form className="register-form">
          <FormControl margin="normal" fullWidth>

          </FormControl>
        </Form>
      </Formik>
    </Paper>
  )

}

export default RegisterForm;
