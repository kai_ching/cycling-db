import React from "react";
import "./NavButton.scss";

const NavButton = ({ onClick }) => {
  return (
    <button className="nav-button" onClick={onClick}>
      Menu
    </button>
  )
}

export default NavButton;
