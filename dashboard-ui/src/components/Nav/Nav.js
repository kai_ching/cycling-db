import React from "react";
import { Link, useLocation } from "react-router-dom";
import classNames from 'classnames';
import "./Nav.scss";
import {useSelector} from "react-redux";

function Nav({routes, selected, variant="NavBar"}) {
  const signedIn = useSelector(store => store.profile.signedIn);
  const permissions = useSelector(store => store.profile.permissions);
  const location = useLocation();

  if(!selected) {
    selected = location.pathname;
  }

  const availableRoutes = routes.filter(route =>
    (signedIn || route.public) &&
    (!route.permissions || route.permissions & permissions > 0) &&
    route.label);
  if(availableRoutes.length > 0) {
    return (
      <nav className={`Nav ${variant}`}>
        <ul>
          {
            availableRoutes.map(route => {
              let routeNavGroup = route.path;
              if(route.props && route.props.navGroup) {
                routeNavGroup = route.props.navGroup;
              }

              const classes = classNames("nav-item", {selected : selected != null && selected === routeNavGroup });
              return (
                <li className={classes} key={route.path}>
                  <Link to={route.path}>{route.label}</Link>
                </li>);
            })
          }
        </ul>
      </nav>
    );
  } else {
    return null;
  }

}

export default Nav;
