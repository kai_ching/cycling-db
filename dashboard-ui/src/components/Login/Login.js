import React, {useState} from "react";
import {Paper, TextField, LinearProgress, Button, Link, FormControl} from "@material-ui/core";

import "./Login.scss";

const Login = ({forgotPasswordUrl, loginAction, submitting}) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const submitHandler = event => {
    event.preventDefault();
    if(typeof loginAction === "function") {
      loginAction(username, password);
    }
  }

  return (
    <Paper className="login" square elevation={2}>
      <form onSubmit={submitHandler}>
        <div>
          <FormControl margin="normal" fullWidth>
            <TextField label="username" name="username" value={username} variant="outlined"
                       onChange={evt => {setUsername(evt.target.value)}}/>
          </FormControl>
          <FormControl margin="normal" fullWidth>
            <TextField label="password" type="password" name="password" value={password} variant="outlined"
                       onChange={evt => { setPassword(evt.target.value) }}/>
          </FormControl>
        </div>
        <div className="actions">
          <Button type="submit" variant="contained" color="primary"
                  disabled={submitting || username.length === 0 || password.length === 0}>
            Login
          </Button>

          { forgotPasswordUrl ? (
            <Link className="forgot-password" href={forgotPasswordUrl}>
              Forgot password
            </Link>
          ) : null}

        </div>
      </form>
      <LinearProgress className={`progress ${submitting ? 'visible': ''}`}/>
    </Paper>
  );
}

export default Login;
