import {
  formatDate, formatDistance, formatDuration, formatPower, formatHeartRate,
  formatSpeed, formatCadence, formatEnergy, formatPercentage
} from "../../formatUtils";
import Paper from "@material-ui/core/Paper";
import React from "react";

import "./ActivityDetail.scss";
import SummaryStat from "../ActivitySummary/SummaryStat";
// import Chart from "../Charts/Chart";

const ActivityDetail = ({activity}) => {
  const summaryStats = {
    "Distance" : `${formatDistance(activity.distance, "km")}`,
    "Elev. Gain": `${formatDistance(activity.ascent)}`,
    "Moving time": `${formatDuration(activity.movingTime)}`
  }

  const detailStats = {};
  if(activity.streams.Speed) {
    detailStats.Speed = [formatSpeed(activity.averageSpeed, "km/h"), formatSpeed(activity.maximumSpeed, "km/h")];
  }
  if(activity.streams.Cadence) {
    detailStats.Cadence = [ formatCadence(activity.averageCadence), formatCadence(activity.maximumCadence)];
  }
  if(activity.streams.Power) {
    detailStats.Power = [ formatPower(activity.averagePower), formatPower(activity.maximumPower)];
  }
  if(activity.streams.HeartRate) {
    detailStats["Heart rate"] = [ formatHeartRate(activity.averageHeartRate), formatHeartRate(activity.maximumHeartRate)];
  }
  if(activity.energy) {
    detailStats["Energy output"] = [ formatEnergy(activity.energy, "kJ")]
  }
  if(activity.idleTime) {
    detailStats["Idle time"] = [ formatDuration(activity.idleTime) +
    ` (${formatPercentage(activity.idleTime / (activity.idleTime + activity.movingTime))})`]
  }

  return (
    <Paper square elevation={2} className="activity-detail">
      <div className="grid">
        <div>
          <span className="date">{formatDate(activity.startTime)}</span>
          <h2>{activity.title}</h2>
          <p className="description">{ activity.description }</p>
        </div>
        <div>
          <div className="summary-stats">
            <ul>
              { Object.keys(summaryStats).map(key => (
                <li key={key}>
                  <SummaryStat label={key} value={summaryStats[key]}/>
                </li>
              ))}
            </ul>
          </div>
          <div className="detail-stats">
            <table>
              <thead>
                <tr>
                  <td/>
                  <td>Avg</td>
                  <td>Max</td>
                </tr>
              </thead>
              <tbody>
                {Object.keys(detailStats).map(stat => (
                  <tr key={stat}>
                    <td>{stat}</td>
                    {detailStats[stat].map((val,index) => <td key={`${stat}-${index}`}>{val}</td>)}
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div>
        {/*<Chart activity={activity}/>*/}
      </div>
    </Paper>
  );
}

export default ActivityDetail;
