import React from "react";
import "./SummaryStat.scss";

const SummaryStat = ({label, value}) => (
    <div className="summary-stat">
      <div className="label"> {label} </div>
      <div className="value"> {value} </div>
    </div>
);

export default SummaryStat;
