import React from 'react';
import { useHistory } from "react-router-dom";
import Paper from "@material-ui/core/Paper";
import "./ActivitySummary.scss";

import {formatDate, formatDuration, formatDistance } from "../../formatUtils";

import SummaryStat from "./SummaryStat";


const ActivitySummary = ({ activity }) => {
  const history = useHistory();
  const summaryStats = {
    "Distance" : `${formatDistance(activity.distance, "km")}`,
    "Elev. Gain": `${formatDistance(activity.ascent)}`,
    "Time": `${formatDuration(activity.movingTime)}`
  }

  const selectActivity = () => {
    history.push(activity["_links"].page);
  }

  return (
    <Paper square elevation={2} className="summary-card" onClick={selectActivity}>
      <div className="card-image">
        { activity.map ? <img src={activity.map} alt="activity route"/> : null }
      </div>
      <div className="card-detail">
        <div>
          <span className="date">{formatDate(activity.startTime)}</span>
          <h3>{ activity.title }</h3>
          <div className="summary-stats">
            <ul>
              { Object.keys(summaryStats).map(key => (
                <li key={key}>
                  <SummaryStat label={key} value={summaryStats[key]}/>
                </li>
              ))}
            </ul>
          </div>
        </div>
        <div className="card-footer"/>
      </div>
    </Paper>
  );
}

export default ActivitySummary;
