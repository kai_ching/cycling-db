import React, {useRef, useState} from 'react';
import './fileUpload.scss';

const FileUpload = (props) => {
  const fileInput = useRef();
  const [dragOver, setDragOver] = useState(false);
  const [files, setFiles] = useState(undefined);

  const placeholder = props.placeholder || 'Click or drop file here';
  const {onFileChanged, multiple} = props;

  let filePrompt = <span>{placeholder}</span>;
  if (files && files.length > 0) {
    filePrompt = (
      <div>
        <span>File: {files[0].name}</span>
        <a href="#upload" onClick={(evt) => {
          evt.preventDefault();
          evt.stopPropagation();
          assignFiles(undefined);
        }}>Remove</a>
      </div>);
  }

  const assignFiles = files => {
    if (files == null) {
      setFiles(undefined);
      onFileChanged(undefined);
    } else if (multiple) {
      setFiles(files);
      onFileChanged(files);
    } else {
      setFiles(files[0]);
      onFileChanged(files[0]);
    }
  }

  return (
    <div className='file-upload'>
      <div className={'file-upload-area' + (dragOver ? ' drag-over' : '')}
           onClick={() => fileInput.current.click()}
           onDragOver={evt => {
             setDragOver(true);
             evt.preventDefault();
           }}
           onDragEnter={() => setDragOver(true)}
           onDragLeave={() => setDragOver(false)}
           onDrop={evt => {
             evt.preventDefault();
             setDragOver(false);
             assignFiles(evt.dataTransfer.files);
           }}>
        {filePrompt}
        <input type="file" ref={fileInput} multiple={multiple} onChange={changeEvent => {
          const files = Array.from(changeEvent.target.files);
          changeEvent.target.value = '';
          assignFiles(files);
        }
        }/>
      </div>
    </div>
  );
};

export default FileUpload;
