export const isPointOnLine = (p, line, tol=5) => {
  let t = ((line.x1 - line.x2)*(p.x - line.x1) + (line.y1 - line.y2)*(p.y - line.y1)) /
    ((line.x2 - line.x1) * (line.x1 - line.x2) + (line.y2 - line.y1) * (line.y1 - line.y2));
  const closestVector = {
    x: p.x - (line.x1 + t * (line.x2 - line.x1)),
    y: p.y - (line.y1 + t * (line.y2 - line.y1))
  }
  return abs(closestVector) <= tol;
}

export const dotProduct = (vector1, vector2) => {
  return vector1.x * vector2.x + vector1.y * vector1.y;
}

export const abs = (vector) => {
  return Math.sqrt(vector.x * vector.x + vector.y * vector.y);
}

export const quadratic = (a, b, c) => {
  const determinant = b * b - 4 * a * c;
  if( determinant < 0) {
    return null;
  } else if(determinant === 0) {
    return [-b / (2 * a)];
  } else {
    return [ (-b + Math.sqrt(determinant)) / (2 * a), (-b - Math.sqrt(determinant))/ (2 * a)];
  }
}
