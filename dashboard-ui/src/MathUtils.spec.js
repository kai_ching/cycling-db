import { isPointOnLine } from "./MathUtils";

describe("Test isPointOnLine calculations", () => {
  test("x-axis alignment", () => {
    const line = { x1: 0, y1: 0, x2: 10, y2: 0};
    const point = { x: 4, y: 0};
    expect(isPointOnLine(point, line, 0)).toBe(true);
  });

  test("y-axis alignment", () => {
    const line = { x1: 0, y1: 0, x2: 0, y2: 10};
    const point = { x: 0, y: 4};
    expect(isPointOnLine(point, line, 0)).toBe(true);
  });

  test("quadrant one", () => {
    const l1 = { x1: 0, y1: 0, x2: 10, y2: 10};
    const point = { x: 5, y: 5};
    expect(isPointOnLine(point, l1, 0)).toBe(true);

    const l2 = { x1: 10, y1: 10, x2: 0, y2: 0};
    expect(isPointOnLine(point, l2, 0)).toBe(true);
  });

  test("quadrant two", () => {
    const line = { x1: 0, y1: 0, x2: -10, y2: 10};
    const point = { x: -5, y: 5};
    expect(isPointOnLine(point, line, 0)).toBe(true);
  });

  test("quadrant three", () => {
    const line = { x1: 0, y1: 0, x2: -10, y2: -10};
    const point = { x: -5, y: -5};
    expect(isPointOnLine(point, line, 0)).toBe(true);
  });

  test("quadrant four", () => {
    const line = { x1: 0, y1: 0, x2: 10, y2: -10};
    const point = { x: 5, y: -5};
    expect(isPointOnLine(point, line, 0)).toBe(true);
  });

  test("example", () => {
    const line = {x1: 402, y1: 113, x2: 656, y2: 286};
    const point = {x: 540, y: 206};
    isPointOnLine(point, line);
  })
})
