const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RepositorySchema = new Schema({
  root: { type: String, required: true, trim: true },
  name: { type: String, required: true, unique: true },
  description : { type: String, trim: true}
});

module.exports = mongoose.model('Repository', RepositorySchema);
