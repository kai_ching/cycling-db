const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AssetSchema = new Schema({
  repository:     { type: String, required: true, trim: true },
  path:           { type: String, required: true, trim: true },
  size:           { type: Number, required: true },
  published:      { type: Boolean, default: false},
  hash:           { type: String, required: true},
  metadata:     { type: Object, default: {}}
});

AssetSchema.options.toJSON = {
  transform: function(doc, ret) {
    delete ret._id;
    delete ret.__v;
    delete ret.path;
  }
}
module.exports = mongoose.model('Asset', AssetSchema);
