const Asset = require('./Asset');
const fs = require('fs/promises');
const fsConstants = require('fs').constants;
const createReadStream = require("fs").createReadStream;
const resolve = require('path').resolve;
const crypto = require('crypto');
const mkdirp = require('mkdirp');

const ObjectId = require("mongoose").Types.ObjectId;


class Repository {

  constructor(name, folderLocation) {
    this.repository = name;
    this.folderLocation = resolve(folderLocation);

    fs.access(this.folderLocation, fsConstants.F_OK | fsConstants.R_OK | fsConstants.X_OK)
      .catch(error => {
        if(error.code === 'ENOENT') {
          mkdirp.sync(this.folderLocation);
        } else {
          console.error(`Cannot open directory ${this.folderLocation}`);
        }
      });
  }

  add(assetPath, metadata, data) {
    const destPath = resolve(this.folderLocation, assetPath);
    let promise;
    if(data) {
      promise = mkdirp(resolve(destPath.substring(0, destPath.lastIndexOf('/'))))
        .then(() => fs.writeFile(destPath, data))
        .then(() => destPath);
    } else {
      promise = fs.access(destPath, fsConstants.F_OK | fsConstants.R_OK)
        .then(() => destPath);
    }
    return promise.then(filePath => {
      const fileStream = createReadStream(filePath);
      const hash = crypto.createHash("sha1");
      hash.setEncoding("hex");

      return new Promise(resolve => {
        fileStream.on("end", () => {
          hash.end();
          resolve({
            path: filePath,
            length : fileStream.bytesRead,
            hash: hash.read()
          });
        });
        fileStream.pipe(hash);
      });
    })
    .then(fileDescriptor => {
      const asset = Asset.create({
        repository : this.repository,
        size: fileDescriptor.length,
        path: fileDescriptor.path,
        hash: fileDescriptor.hash,
        metadata
      });
      return asset;
    });
  }

  async remove(asset) {
    try {
      if(asset instanceof ObjectId) {
        asset = await Asset.findById(asset);
      }

      if(asset) {
        await fs.unlink(resolve(this.folderLocation, asset.path));
        await asset.remove();
      }
    } catch(error) {
      console.error(error);
    }
  }

  find(query) {
    return Asset.find(query);
  }

  async publish(asset) {
    if(typeof asset === 'string') {
      asset = ObjectId(asset);
    }
    if(asset instanceof ObjectId) {
      return Asset.findOneAndUpdate({
        _id: asset,
        published: false
      }, {
        $set: {
          published: true
        }
      });
    } else if(asset instanceof Asset) {
      asset.published = true;
      return asset.save();
    }
    return Promise.resolve();
  }

  async unpublish(asset) {
    if(typeof asset === 'string') {
      asset = ObjectId(asset);
    }
    if(asset instanceof ObjectId) {
      return Asset.findOneAndUpdate({
        _id: asset,
        published: true
      }, {
        $set: {
          published: false
        }
      });
    } else if(asset instanceof Asset) {
      asset.published = true;
      return asset.save();
    }
    return Promise.resolve();
  }
}

module.exports = {
  name: 'repository',
  settings: {
    repositoryName: 'activities',
    folderLocation: 'tmp/data/activities',
    publicUrl: "/assets"
  },

  created() {
    this.repository = new Repository(this.settings.repositoryName, this.settings.folderLocation);
  },
  actions: {
    add(ctx) {
      const { data, assetPath, metadata } = ctx.params;
      return this.repository.add(assetPath, metadata, data);
    },
    remove(ctx) {
      const { sourceId } = ctx.params;
      this.unpublish(sourceId);
      return this.repository.remove(sourceId);
    },
    async find(ctx) {
      const { id, publishedOnly, ...rest } = ctx.params;
      const query = { ...rest };
      if(publishedOnly) {
        query.published = true;
      }
      if(id) {
        query["_id"] = ObjectId(id);
      }
      const assets = await this.repository.find(query);
      return assets.map(asset => {
        asset.location = this.settings.publicUrl + asset.id;
        return asset;
      });
    },
    publish(ctx) {
      const { sourceId } = ctx.params;
      return this.repository.publish(sourceId);
    },
    unpublish(ctx) {
      const { sourceId } = ctx.params;
      return this.repository.unpublish(sourceId);
    }
  }

}
