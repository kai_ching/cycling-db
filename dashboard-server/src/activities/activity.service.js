const SportsLib = require("@sports-alliance/sports-lib/lib/index").SportsLib;
const DbService = require('moleculer-db');
const MongooseAdapter = require('moleculer-db-adapter-mongoose');
const Activity = require('./Activity');
const appConfig = require("../appConfig");
const ObjectId = require("mongoose").Types.ObjectId;
const { MoleculerError } = require("moleculer").Errors;

const round = require("../MathUtil").round;

function getActivityStat(activity, statName) {
  if(activity.stats.get(statName)) {
    return activity.stats.get(statName).value;
  }
  return undefined;
}

function parseStreams(activity, logger) {
  const allowableStreams = activity.streams.filter(stream => {
    const streamType = stream ? stream.type : undefined;
    if(streamType && appConfig.activities.allowableStreams.indexOf(streamType) === -1) {
      logger.debug('Ignoring stream in file: ' + streamType);
    }
    return appConfig.activities.allowableStreams.indexOf(stream.type) > -1
  });
  const maxStreamLength = allowableStreams.map(stream => stream.data.length).reduce((result, length) => Math.max(result, length), 0);
  const timestamps = [];
  const result =new Map();

  for(let i = 0; i < maxStreamLength; i++) {
    const shouldInclude = allowableStreams
      .map(stream => stream.data[i])
      .reduce((result, streamValue) => result || (streamValue !== undefined && streamValue !== null), false);
    if(shouldInclude) {
      timestamps.push(i);
    }
  }

  allowableStreams.forEach(stream => {
    const data = [];
    timestamps.forEach(time => {
      // TODO : hack for rounding latlng
      if(stream.type === "Latitude" || stream.type === "Longitude") {
        data.push(round(stream.data[time], 6));
      } else {
        data.push(stream.data[time])
      }
    });
    result.set(stream.type, data);
  });
  result.set('timestamps', timestamps);
  return result;
}

module.exports = {
  name: 'activity',
  mixins: [ DbService ],
  adapter: new MongooseAdapter(process.env.MONGODB_URL),
  model: Activity,
  settings: {
    pageSize: 20,
    maxPageSize: 100
  },
  actions: {
    create(ctx) {
      const {id, title, description, file, source } = ctx.params;
      return SportsLib.importFromFit(file.data)
        .then(event => ({
          _id: id,
          title: title,
          description: description,
          sourceId: source._id,
          type: event.stats.get('Activity Types').value[0],
          deviceName: event.stats.get('Device Names').value[0],
          startTime: event.activities[0].startDate,
          endTime: event.activities[0].startDate,
          movingTime: getActivityStat(event.activities[0], 'Moving time'),
          idleTime: getActivityStat(event.activities[0], 'Pause Time'),
          distance: getActivityStat(event.activities[0], 'Distance'),
          averageSpeed: getActivityStat(event.activities[0], 'Average Speed'),
          maximumSpeed: getActivityStat(event.activities[0], 'Maximum Speed'),
          averageCadence: getActivityStat(event.activities[0], 'Average Cadence'),
          maximumCadence: getActivityStat(event.activities[0], 'Maximum Cadence'),
          ascent: getActivityStat(event.activities[0], 'Ascent'),
          descent: getActivityStat(event.activities[0], 'Descent'),
          energy: getActivityStat(event.activities[0], 'Energy'),
          averageTemperature: getActivityStat(event.activities[0], 'Average Temperature'),
          status: 'pending',
          createdAt: new Date(),
          streams: parseStreams(event.activities[0], ctx.broker.logger)
        }))
        .then(activity => ctx.call('activity.save', { activity: activity }))
        .then(activity => {
          ctx.call('scheduling.defer', {service: 'dummy.hello', context: {activity: activity}, maxAttempts: 3, timeout: 300 });
          return activity
        })
        .catch(error => {
          console.error(error);
          throw {
            message : "Failed to create activity",
            source  : source
          }
        });
    },
    getRecent(ctx) {
      return this.Promise.resolve(ctx)
        .then(ctx => {
          const owner = ctx.params.owner;
          let {page, pageSize} = ctx.params;
          if(page == null) {
            page = 0;
          }
          if(pageSize == null) {
            pageSize = 20;
          }
          return ctx.call('activity.find', {
            query: {
              owner: owner,
              status: 'complete'
            },
            sort: '-startTime',
            fields: appConfig.activities.summaryFields,
            offset: page * pageSize,
            limit: pageSize
          });
        });
    },
    getByDate(ctx) {
      return this.Promise.resolve(ctx)
        .then(ctx => {
          const {owner, page, pageSize} = ctx.params;
          let {from, to} = ctx.params;
          if(from == null && to == null) {
            to = new Date();
          }
          if(from == null) {
            from = new Date(to.getDate() - 7);
          } else if(to == null) {
            to = new Date(from.getDate() + 7);
          }

          const findParams = {
            query: {
              owner: owner,
              status: 'complete',
              $or: [
                { startTime: { $gte: from, $lte: to } },
                { endTime: { $gte: from, $lte: to } }]
            },
            sort: '-startTime',
            fields: appConfig.activities.summaryFields
          }
          if(page !== undefined && pageSize !== undefined) {
            findParams.offset = page * pageSize;
            findParams.limit = pageSize;
          }
          return ctx.call('activity.find', findParams);
        });
    },
    getById(ctx) {
      const activityId = ObjectId(ctx.params.id);
      return Promise.resolve(ctx)
        .then(ctx => ctx.call('activity.find', {query: {_id : activityId}}))
        .then(result => {
          if(result.length === 1) {
            return result[0];
          } else if(result.length === 0) {
            throw new MoleculerError("No activity", 404, "notFound", {id: activityId.toString()});
          } else
            throw new MoleculerError("Multiple activities", 500, "multipleActivities", {id: activityId.toString()});
        })
    },
    save(ctx) {
      return this.Promise.resolve(ctx)
        .then(ctx => ctx.params)
        .then(params => {
          const activity = params.activity;
          if(activity.status !== 'deleted') {
            if(activity.streams !== undefined &&
               activity.title != null) {
              activity.status = 'complete';
            } else {
              activity.status = 'pending';
            }
          }
          return params;
        })
        .then(params => {
          if(params.id) {
            return this.adapter.updateById(params.id, params.activity);
          } else {
            return this.adapter.insert(params.activity);
          }
        });
    },
    delete(ctx) {
      return this.Promise.resolve(ctx)
        .then(ctx => ctx.params.id)
        .then(id => this.adapter.removeById(id));
    }
  }
};
