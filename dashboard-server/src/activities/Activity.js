const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const ActivitySchema = new Schema({
  owner:              {type: Schema.Types.ObjectId},
  title:              {type: String, trim: true},
  description:        {type: String, trim: true},
  type:               {type: String, trim: true},
  startTime:          {type: Date},
  endTime:            {type: Date},
  movingTime:         {type: Number},
  idleTime:           {type: Number},
  distance:           {type: Number},
  averageSpeed:       {type: Number},
  maximumSpeed:       {type: Number},
  averageCadence:     {type: Number},
  maximumCadence:     {type: Number},
  ascent:             {type: Number},
  descent:            {type: Number},
  energy:             {type: Number},
  averageTemperature: {type: Number, of: [Number]},
  streams:            {type: Map},
  createdAt:          {type: Date, required: true},
  status:             {type: String, required: true},
  sourceId:           {type: Schema.Types.ObjectId},
  map:                {type: String},
  images:             {type: [String], default: []},
  tags:               {type: [String], default: []}
}, {
  versionKey: false
});

ActivitySchema.virtual('published').get(() => this.properties.status === 'completed');

ActivitySchema.options = {
  toObject: {
    transform: function(doc, ret) {
      ret.id = doc.id;
      delete ret._id;
    }
  }
}

module.exports = mongoose.model('Activity', ActivitySchema);
