exports.stringify = function(contentType, payload) {
  const endIndex = contentType.indexOf(";") > -1 ? contentType.indexOf(";") : contentType.length;
  contentType = contentType.substring(0, endIndex);

  if(typeof payload === "string" || payload instanceof Buffer ) {
    return payload;
  }

  switch(contentType) {
    case "application/json" : return JSON.stringify(payload);
    default: return JSON.stringify(payload);
  }
}
