require('dotenv').config();
const glob = require('glob');
const { ServiceBroker } = require('moleculer');


const broker = new ServiceBroker({
  logger: console,
  logLevel: process.env.LOG_LEVEL || 'info',
  hotReload: true
});

glob('src/**/*.service.js', function(err, files) {
  files.forEach(file => {
    broker.loadService(file);
  });
  broker.start();
});
