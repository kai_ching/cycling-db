const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const JobSchema = new Schema({
  owner:          { type: Schema.Types.ObjectId },
  service:          { type: String, trim: true, required: true},
  context:        { type: Object, required: true },
  createdAt:      { type: Date, default: new Date()},
  lastExecutedAt: { type: Date},
  status:         { type: String, default: 'pending'},
  attempts:       { type: Number, default: 0 },
  maxAttempts:    { type: Number },
  timeout:        { type: Number }
}, { versionKey : false});

JobSchema.virtual('priority').get(() =>
  (this.properties.lastExecutedAt ? this.properties.lastExecutedAt.getTime() : this.properties.createdAt.getTime()));

module.exports = mongoose.model('Job', JobSchema);
