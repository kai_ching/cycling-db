const DbService = require('moleculer-db');
const MongooseAdapter = require('moleculer-db-adapter-mongoose');
const Job = require('./Job');

const execWithTimeout = (func, timeout) => {
  return new Promise((resolve, reject) => {
    let tId = undefined;
    if(timeout && timeout > 0) {
      tId = setTimeout(() => {
        console.log('Operation timed out');
        reject({ message: "Operation timed out"});
      }, timeout * 1000);
    }
    try {
      const returnObj = func();
      if(returnObj.then instanceof Function) {
        returnObj.then(result => {
          clearTimeout(tId);
          resolve(result);
        }) .catch(error => {
          clearTimeout(tId);
          reject(error);
        });
      } else {
        clearTimeout(tId);
        resolve(returnObj);
      }
    } catch (error) {
      reject(error);
    }
  });
}

module.exports = {
  name: 'scheduling',
  mixins: [ DbService ],
  adapter: new MongooseAdapter(process.env.MONGODB_URL),
  model: Job,
  settings: {
    pageSize: 10,
    maxPageSize: 10
  },

  actions: {
    defer(ctx) {
      const { service, context, maxAttempts, timeout} = ctx.params;
      return this.adapter.insert({
        service, context, maxAttempts, timeout
      }).then(job => {
          this.execute(ctx);
          return job._id;
        });
    },
    purge(ctx) {
      return this.adapter.removeMany({});
    }
  },

  methods: {
    execute(ctx) {
      this._find(ctx, {
        query: { status: 'pending' },
        sort: '-priority',
        limit: 1
      })
        .then(jobs => {
          if(jobs.length > 0) {
            const job = jobs[0];
            return execWithTimeout(() => (ctx.call(job.service, job.context)), job.timeout)
              .then(() => this._remove(ctx, { id: job._id }))
              .catch(error => {
                job.status = 'pending';
                job.attempts++;
                if(job.maxAttempts && job.attempts >= job.maxAttempts) {
                  return this._remove(ctx, { id: job._id});
                } else {
                  return this._update(ctx, job);
                }
              })
              .finally(() => this.execute(ctx));
          }
        });
    }
  }
}
