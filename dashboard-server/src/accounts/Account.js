const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AccountSchema = new Schema({
  _id: { type: String },
  provider: { type: String, required: true, default: "password"},
  hash: { type: String, required: true },
  refreshToken: { type: String, default: null},
  lastLoggedIn: { type: Date, default: null},
  groups: { type: [Schema.Types.ObjectId], default: []},
  permissions: { type: Number, default: 0 },
  displayName: { type: String},
  enabled: { type : Boolean, default: true },
  email: { type: String },
  photo: { type: String }
});

module.exports = mongoose.model('Account', AccountSchema);
