const bcrypt = require("bcrypt");

const Account = require("./Account");
const { MoleculerError } = require("moleculer").Errors;

const authSettings = require("../appConfig").authentication;

const checkPermission = async (authorisedBy, requiredPermission) => {
  const author = Account.findById(authorisedBy);
  const hasPermissions = !requiredPermission || (author.permissions & requiredPermission);
  if(author && hasPermissions) {
    return;
  } else throw new MoleculerError("Unauthorized operation", 401, "unauthorizedOperation");
}

class AccountService {

  static PERMISSIONS = {
    userAdmin : 0x01
  }

  constructor(authSettings) {
    if(authSettings.defaultUsers) {
      authSettings.defaultUsers.forEach(entry => {
        const { username, password } = entry;
        Account.findOne({"_id" : username})
          .then(account => {
            if(account == null) {
              return this.createUser(username, {provider: "local", password})
                .then(account => this.grant(username, 0x01))
                .catch(error => {
                  console.error(error);
                });
            }
          });
      });
    }
  }

  async authenticate(credentials) {
    if(credentials) {
      let authenticated = undefined;
      switch(credentials.authProvider) {
        case "local" : {
          const { username, password } = credentials;
          const account = await Account.findOne({ _id: username });
          if(account && bcrypt.compareSync(password, account.hash)) {
            authenticated = account;
          }
          break;
        }
        case "jwt" : {
          const { token } = credentials;
          const account = await Account.find({_id: token.sub});
          if(account) {
            authenticated =  account;
          }
          break;
        }
        case "refresh" : {
          const { token } = credentials;
          if(token) {
            const tokenParts = Buffer.from(token, "base64").toString().split(":");
            const account = await Account.findOne({ "_id": tokenParts[0]});
            if(account) {
              const expired = (Date.now() - account.lastLoggedIn.getTime()) > 24 * 3600 * 1000;
              if(!expired && bcrypt.compareSync(tokenParts[1], account.refreshToken)) {
                authenticated = account;
              }
            }
          }

          break;
        }
        default: throw new MoleculerError("authProvider field missing", 400, "noAuthProvider");
      }
      if(authenticated) {
        const refreshToken = Math.random().toString(36).substring(2);
        authenticated.lastLoggedIn = new Date();
        authenticated.refreshToken = bcrypt.hashSync(refreshToken, 10);
        authenticated = await authenticated.save();
        return {
          account: authenticated,
          refreshToken: Buffer.from(`${authenticated["_id"]}:${refreshToken}`).toString("base64")
        }
      }
    }
    throw new MoleculerError("Invalid credentials", 401, "invalidCredentials");
  }

  async createUser(username, credentials) {
    if(!credentials.provider || !credentials.password) {
      throw new MoleculerError("Invalid credentials", 401, "invalidCredentials");
    }

    let account = await Account.findById(username);
    if(!account) {
      bcrypt.hash(credentials.password, 10, async function(err, hash) {
        account = await Account.create({
          _id: username,
          provider: credentials.provider,
          hash: hash
        });
        return account;
      });
    } else {
      throw new MoleculerError("User exists", 400, "userExists", { username: username});
    }
  }

  async setUserEnabled(username, enabled) {
    return Account.findOneAndUpdate({ _id: username }, {
      $set : { enabled : enabled}
    });
  }

  async grant(username, permission) {
    const account = await Account.findById(username);
    if(account) {
      account.permissions |= permission;
      return account.save();
    } else throw new MoleculerError("Not found", 404, "userNotFound");
  }

  async revoke(username, permissions) {
    const account = await Account.findById(username);
    if(account) {
      account.permissions &= (~permissions);
      return account.save();
    } else
      throw new MoleculerError("Not found", 404, "userNotFound");
  }
}

module.exports = {
  name: "account",
  created() {
    this.service = new AccountService(authSettings);
  },
  actions: {
    authenticate(ctx) {
      return Promise.resolve(this.service.authenticate(ctx.params));
    },
    async createUser(ctx) {
      const { username, credentials, authorisedBy } = ctx.params;
      return checkPermission(authorisedBy, AccountService.PERMISSIONS.userAdmin)
        .then(() => this.service.createUser(username, credentials));
    },
    async enableUser(ctx) {
      const { username, authorisedBy } = ctx.params;
      return checkPermission(authorisedBy, AccountService.PERMISSIONS.userAdmin)
        .then(() => this.service.setUserEnabled(username, true));
    },
    async disableUser(ctx) {
      const { username, authorisedBy } = ctx.params;
      return checkPermission(authorisedBy, AccountService.PERMISSIONS.userAdmin)
        .then(() => this.service.setUserEnabled(username, false));
    },
    async grant(ctx) {
      const { username, permissions, authorisedBy } = ctx.params;
      return checkPermission(authorisedBy, AccountService.PERMISSIONS.userAdmin)
        .then(() => this.service.grant(username, permissions));
    },
    async revoke(ctx) {
      const { username, permissions, authorisedBy } = ctx.params;
      return checkPermission(authorisedBy, AccountService.PERMISSIONS.userAdmin)
        .then(() => this.service.revoke(username, permissions));
    }
  }
}
