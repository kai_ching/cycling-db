const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const GroupSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String },
  permissions: { type: Number, default: 0 }
});

module.exports = mongoose.model("Group", GroupSchema);
