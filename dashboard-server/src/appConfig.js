const fs = require('fs');
const YAML = require('yaml');
module.exports = YAML.parse(fs.readFileSync('./config.yaml', 'utf-8'));
