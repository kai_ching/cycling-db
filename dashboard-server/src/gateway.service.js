const os = require('os');
const fs = require('fs');
const mkdirp = require('mkdirp');

const web = require('moleculer-web');
const compression = require('compression');
const cookieParser = require("cookie-parser");
const BusBoy = require('busboy');

const jwt = require("jsonwebtoken");
const mongoose = require('mongoose');

const authSettings = require("./appConfig").authentication;
const { parseCookie, stringifyCookie} = require("./HttpUtils");

const sendError = (req, res, err) => {
  // Return with the error as JSON object
  res.writeHead(err.code, {
    "Content-Type" : "application/json; charset=utf-8"
  });
  res.end(JSON.stringify({ message : err.message }));
}

module.exports = {
  name: 'gateway',
  mixins: [ web ],
  settings: {
    port: process.env.PORT || 3000,
    routes:[
      {
        path: '/api/activities',
        authorization: false,
        uses: [ compression()],
        aliases: {
          'GET /' (req, res) { return this.handleListActivities(req, res) },
          'GET /:id' (req, res) { return this.handleGetActivity(req, res) },
          'POST /' (req, res) { return this.handleCreateActivity(req, res) },
          'DELETE /:id': 'activity.delete',
        },
        cors: true,
        bodyParsers: {
          json: { strict: true }
        }
      },
      {
        path: "/api/login",
        uses: [ compression() ],
        authentication: true,
        aliases: {
          "POST /"(req, res) {
            const account = res.$ctx.meta.user;
            if(account) {
              const jwtToken = jwt.sign({
                sub: account.username,
                permissions: account.permissions
              }, authSettings.jwtSecret);
              res.writeHead(200, "OK", {
                "Content-Type": "application/json",
                "Set-Cookie": stringifyCookie({"refreshToken": res.$ctx.meta.refreshToken}, true)
              });
              res.end(JSON.stringify({
                token: jwtToken,
                profile: {
                  permissions: account.permissions || 0,
                  displayName: account.displayName,
                  photoURL: account.photo
                }
              }));
            } else {
              sendError(req, res, { message: "invalidCredentials" });
            }
          }
        },
        bodyParsers: {
          json: true
        }
      },
      {
        path: "/api/register",
        uses: [ compression() ],
        authentication: false,
        authorization: false,

        bodyParsers: {
          json: true
        }
      },
      {
        path: "/assets",
        uses: [ compression()],
        aliases: {
          "GET /:id" (req, res) {
            return this.retrieveAsset(req, res)
          }
        }
      }
    ],
    onError: sendError
  },
  methods: {
    authenticate(ctx, route, req, res) {
      const credentials = req.body;
      if(credentials.authProvider === "refresh") {
        const cookieValues = parseCookie(req.headers["cookie"]);
        credentials.token = cookieValues.refreshToken;
      }
      return this.broker.call("account.authenticate", credentials)
        .then(({account, refreshToken})=> {
          ctx.meta.user = account;
          ctx.meta.refreshToken = refreshToken;
          return account;
        }).catch(error => {
          return Promise.reject(error);
        });
    },
    authorize(ctx, route, req, res) {
      const authHeader = req.headers["Authorization"];
      if(authHeader && authHeader.startsWith("Bearer ")) {
        const jwtPayload = jwt.verify(authHeader.slice(7), authSettings.jwtSecret);
        return this.broker.call("account.authenticate", {
          "authProvider": "jwt",
          "token" : jwtPayload
        }).then(account => {
          ctx.meta.user = account;
          return account;
        });
      } else {
        return Promise.reject({
          code: 401, message: "invalidCredentials"
        })
      }
    },
    handleListActivities(req, res) {
      Promise.resolve(req.$params)
        .then(params => {
          const {from ,to} = params;
          if(from !== undefined || to !== undefined) {
            return req.$ctx.call('activity.getByDate', req.$params)
          } else {
            return req.$ctx.call('activity.getRecent', req.$params);
          }
        })
        .then(activities => activities.map(activity => {
          activity['_links'] = {
            self: `/api/activities/${activity['_id']}`,
            page: `/activities/${activity["_id"]}`,
            source: `/assets/${activity.sourceId}`
          };
          activity.id = activity['_id'];
          delete activity['_id'];
          return activity;
        }))
        .then(activities => {
          res.writeHead(200, 'OK', {'Content-Type': 'application/json'});
          res.end(JSON.stringify(activities));
        });
    },
    handleGetActivity(req, res) {
      return req.$ctx.call('activity.getById', req.$params)
        .then(activity => {
          res.writeHeader(200, 'OK', {"Content-type": "application/json; charset=utf-8"});
          const body = {
            ...activity,
            "_links": {
              self: `/api/activities/${activity._id}`,
              page: `/activities/${activity.id}`,
              source: `/assets/${activity.sourceId}`
            }
          }
          delete body.sourceId;
          res.end(JSON.stringify(body));
          return this;
        })
        .catch(error => {
          if(error.message.indexOf('Argument passed') === 0) {
            error.code = 400;
          }
          sendError(req, res, error);
        })
    },
    handleCreateActivity(req, res) {
      return new Promise((resolve, reject) => {
          let payload = {};
          const busboy = new BusBoy({ headers: req.headers, limits: {file: '2mb' } });
          busboy.on('file', function(fieldName, file, fileName, encoding, contentType) {
            const tmpFile = req.$service.settings.tmpDir +  Math.random().toString(36).substring(8);
            payload[fieldName] = {
              fileName, contentType, encoding, length: 0
            };
            payload[fieldName].sourceFile = tmpFile;

            file.on('data', function(data) {
              fs.appendFileSync(tmpFile, data);
            });
            file.on('end', function() {
              payload[fieldName].data = fs.readFileSync(payload[fieldName].sourceFile);
              payload[fieldName].size = payload[fieldName].data.length;
            });
          });
          busboy.on('field', function(fieldName, val, fieldNameTruncated, valTruncated, encoding, contentType) {
            if(contentType === 'application/json') {
              payload = Object.assign(payload, JSON.parse(val));
            } else {
              payload[fieldName] = val;
            }

          });
          busboy.on('finish', function() {
            resolve(payload);
          });
          req.pipe(busboy);
        })
        .then(async payload => {
          payload.id = mongoose.Types.ObjectId();
          const asset = await req.$ctx.call('repository.add', {
            data: payload.file.data,
            assetPath: `${payload.id}/sourceFile`,
            metadata: {
              fileName: payload.file.fileName,
              contentType: payload.file.contentType,
              size: payload.file.size,
              encoding: payload.file.encoding
            }
          });
          payload.source = asset;
          return payload;
        })
        .then(payload => req.$ctx.call('activity.create', payload))
        .then(async activity => {
          await req.$ctx.call('repository.publish', { sourceId : activity.sourceId });
          res.writeHeader(200, 'OK', {"Content-type": "application/json; charset=utf-8"});
          res.end(JSON.stringify({
            "id" : activity.id,
            "_links": {
              self: `/api/activities/${activity.id}`,
              page: `/activities/${activity.id}`,
              source: `/assets/${activity.sourceId}`
            }
          }));
        })
        .catch(async error => {
          const sourceId = error.source.id;
          await req.$ctx.call('repository.remove', { sourceId });
          res.writeHeader(500, 'Internal Server Error');
          res.end(error.message);
        })
    },
    retrieveAsset(req, res) {
      return req.$ctx.call('repository.find', req.$params)
        .then(assets => assets.length === 1 ? assets[0] : null )
        .then(asset => {
          if(asset) {
            const contentType = asset.metadata.contentType ? asset.metadata.contentType : "application/octet-stream";
            const fileStream = fs.createReadStream(asset.path);
            // TODO : handle missing file

            const header = {
              'Content-Type': contentType,
              'Content-Length': asset.size
            };
            if(asset.metadata.fileName) {
              header["Content-Disposition"] = `filename=${asset.metadata.fileName}`
            }
            res.writeHead(200, header);
            if(fileStream) {
              fileStream.pipe(res);
            }
          } else {
            res.writeHead(404).end();
          }
        })
    }
  },
  created() {
    const tmpDir = `${os.tmpdir()}/cyclingdb/`
    this.settings.tmpDir = tmpDir;
    return mkdirp(tmpDir);
  }
};
