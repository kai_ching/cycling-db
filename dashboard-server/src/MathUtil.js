exports.round = function(value, precision) {
  const scale = Math.pow(10, precision);
  return Math.round(value * scale) / scale;
}
