exports.parseCookie = (cookieString) => {
  const cookie = {};
  if(typeof cookieString === "string") {
    cookieString.split(";").forEach(entry => {
      const splitIndex = entry.indexOf("=");
      cookie[entry.substring(0, splitIndex).trim()] = entry.substring(splitIndex + 1);
    });
  }
  return cookie;
}

exports.stringifyCookie = (cookieObject, httpOnly, sameSite, secure) => {
  if(cookieObject != null) {
    let cookieOptions = httpOnly ? "httpOnly" : ""
    if(sameSite) {
      cookieOptions += (`; SameSite=${sameSite}`);
    }
    if(secure) {
      cookieOptions += ("; Secure");
    }
    return Object.keys(cookieObject).map(
      cookieKey => `${cookieKey}=${cookieObject[cookieKey]}`)
      .reduce((result, element) => (element + ";" + result), cookieOptions);
  }
  return null;
}
